
enum window_type {
	HANN,
	HAMMING,
	WELCH,
	COSINE
};

struct wav_header {
	const char riff[4];
	int32_t file_size;
	const char file_type[4];
	const char fmt[4];
	int32_t fmt_len;
	int16_t fmt_type;
	int16_t num_chan;
	int32_t srate;
	int32_t sr_bits_chan;
	int16_t bits_chan;
	int16_t bits_per_sample;
	const char data[4];
	int32_t data_size;
};

struct wav {
	struct wav_header *header;
	int16_t *data;
	int32_t num_frames;
	int32_t current_frame;
};

struct wav_header *
new_wav_header(int32_t srate, int16_t num_chan, int16_t bits_per_sample);

struct wav_header *
new_wav_header_from_header(struct wav_header *header);

struct wav *
new_wav(struct wav_header *header, double num_seconds);

struct wav *
new_wav_from_file(FILE *file);

void
free_wav(struct wav *wf);

void
write_wav(struct wav *wf, const char *name);

void
print_wav(struct wav *wf);

void
normalize(int64_t *data, int length, int64_t normal);

void
window(int16_t *signal, int window_size, enum window_type type);

void
window64(int64_t *signal, int window_size, enum window_type type);

/*int64_t *
convolve(
	int64_t *signal,
	int signal_len,
	int64_t *kernel,
	int offset,
	float num_seconds,
	int srate,
	int impulse
);*/

int64_t *
convolve(
	int64_t *signal,
	int offset,
	int length,
	int64_t *signal2,
	int offset2,
	int length2,
	int srate,
	int impulse
);

int64_t *
convolve2(
	int64_t *signal,
	int64_t *signal2,
	int len,
	int window_size,
	int srate
);
