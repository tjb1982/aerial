CC := clang
CFLAGS := -Wall
LIBS := -lm -lfftw3

SRC := $(wildcard *.c)

alysha: $(SRC)
	$(CC) -o alysha $^ $(CFLAGS) $(LIBS)
#	$(CC) $*.c -o $* $(CFLAGS)
