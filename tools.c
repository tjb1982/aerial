#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdint.h>
#include <fftw3.h>
#include "tools.h"


struct wav_header *
new_wav_header(int32_t srate, int16_t num_chan, int16_t bits_per_sample) {
	struct wav_header *new = calloc(1, sizeof(struct wav_header));
	memcpy((void *)new->riff, "RIFF", 4);
	new->file_size = 0;
	memcpy((void *)new->file_type, "WAVE", 4);
	memcpy((void *)new->fmt, "fmt ", 4);
	new->fmt_len = 16;
	new->fmt_type = 1;
	new->num_chan = num_chan;
	new->srate = srate;
	new->sr_bits_chan = (srate * bits_per_sample * num_chan) / 8;
	new->bits_chan = (bits_per_sample * num_chan) / 8;
	new->bits_per_sample = bits_per_sample;
	memcpy((void *)new->data, "data", 4);
	new->data_size = 0;
	return new;
}

struct wav_header *
new_wav_header_from_header(struct wav_header *header) {
	struct wav_header *new = malloc(sizeof(struct wav_header));
	memcpy(new, header, sizeof(struct wav_header));
	return new;
}

struct wav *
new_wav(struct wav_header *header, double num_seconds) {
	struct wav *wf = malloc(sizeof(struct wav));
	wf->header = header;
	header->data_size = num_seconds * header->sr_bits_chan;
	header->file_size = header->data_size + 44;
	wf->data = calloc(header->srate * num_seconds, header->bits_chan);
	wf->num_frames = num_seconds * header->srate;
	wf->current_frame = 0;
	return wf;
}

struct wav *
new_wav_from_file(FILE *file) {
	struct wav_header *header = malloc(sizeof(struct wav_header));
	struct wav *wf;

	fread(header, sizeof(struct wav_header), 1, file);

	wf = new_wav(
		header,
		header->data_size / header->sr_bits_chan
		//(header->data_size / header->bits_chan) / header->srate
	);
	wf->data = malloc(sizeof(uint8_t) * header->data_size);
	fread(wf->data, sizeof(uint8_t), header->data_size, file);

	wf->current_frame = 0;

	return wf;
}

void
free_wav(struct wav *wf) {
	free(wf->header);
	if (wf->data) free(wf->data);
	free(wf);
}

void
write_wav(struct wav *wf, const char *name) {
	FILE *outfile = fopen(name, "w");
	wf->header->file_size = wf->header->data_size + 44;
	fwrite(wf->header, sizeof(struct wav_header), 1, outfile);
	fwrite(wf->data, sizeof(uint8_t), wf->header->data_size, outfile);
	fclose(outfile);
}

void
print_wav(struct wav *wf) {
	for (int i = 0; i < wf->header->data_size/sizeof(int16_t); i++) {
		printf("%i ", wf->data[i]);
	}
	printf("\n");
}

void
normalize(int64_t *data, int length, int64_t normal) {
	int64_t max = 0;
	for (int i = 0; i < length; i++) {
		max = max < labs(data[i]) ? labs(data[i]) : max;
	}
	double x = normal/(double)max;
	for (int i = 0; i < length; i++) {
		data[i] *= x;
	}
}

void
window(int16_t *signal, int window_size, enum window_type type) {
	for (int i = 0; i < window_size; i++) {
		switch (type) {
		case HANN:
			signal[i] *= (0.5 * (1 - cos((2 * M_PI * i) / (window_size - 1.))));
			break;
		case HAMMING:
			signal[i] *= (
				0.54 - ((1 - 0.54) * cos((2 * M_PI * i) / (window_size - 1.)))
			);
			break;
		case WELCH:
			signal[i] *= 1 - pow((i - ((window_size - 1)/2.)) / ((window_size - 1)/2.), 2);
			break;
		case COSINE:
			signal[i] *= sin((M_PI * i) / (window_size - 1.));
			break;
		default:
			break;
		}
	}
}

void
window64(int64_t *signal, int window_size, enum window_type type) {
	for (int i = 0; i < window_size; i++) {
		switch (type) {
		case HANN:
			signal[i] *= (0.5 * (1 - cos((2 * M_PI * i) / (window_size - 1.))));
			break;
		case HAMMING:
			signal[i] *= (
				0.54 - ((1 - 0.54) * cos((2 * M_PI * i) / (window_size - 1.)))
			);
			break;
		case WELCH:
			signal[i] *= 1 - pow((i - ((window_size - 1)/2.)) / ((window_size - 1)/2.), 2);
			break;
		case COSINE:
			signal[i] *= sin((M_PI * i) / (window_size - 1.));
			break;
		default:
			break;
		}
	}
}

/*int64_t *
convolve2(
	int64_t *signal,
	int64_t *signal2,
	int len,
	int window_size,
	int srate
) {
	// for each dft window of each signal, convolve and increment the window until finished
	int64_t *resp = malloc(sizeof(int64_t) * len);
	int num_iterations = len/window_size;
	int overlap_factor = 4;
	for (int k = 0; k < overlap_factor; k++) {
		int impulse = 1;
		for (int i = 0; i < num_iterations; i++) {
			int offset = (window_size * i) + ((window_size/overlap_factor) * k);
			int64_t *new = convolve(signal + offset, window_size, signal2 + offset, 0, window_size/(float)srate, srate, (impulse += 30000));
			printf("impulse: %i\n", impulse);
			window64(new, window_size, HANN);
			for (int j = 0; j < window_size; j++) {
				resp[offset + j] += new[j];
			}
			fftw_free(new);
		}
	}

	return resp;
}*/

int64_t *
convolve(
	int64_t *signal,
	int offset,
	int length,
	int64_t *signal2,
	int offset2,
	int length2,
	int srate,
	int impulse
) {

    fprintf(stderr, "%i\n", length2);
	int64_t *kernel = fftw_malloc(sizeof(int64_t) * length2);
	for (int i = 0; i < length2; i++) {
		kernel[i] = signal2[offset2 + i];
	}
	//window64(kernel, length2, HANN);

	/** forward DFT for `length` of `signal` at `offset` **/
	int dft_size = length;
	double *time1 = fftw_malloc(sizeof(double) * dft_size);
	for (int i = 0; i < dft_size; i++) {
		time1[i] = signal[offset + i];
	}
	fftw_complex *freq1 = fftw_malloc(sizeof(fftw_complex) * ((dft_size/2) + 1));
	fftw_plan pf1 = fftw_plan_dft_r2c_1d(dft_size, time1, freq1, FFTW_ESTIMATE);
	fftw_execute(pf1);

	/** forward DFT for `length2` of `signal2` at `offset2` **/
	double *time2 = fftw_malloc(sizeof(double) * dft_size);
	for (int i = 0; i < length2; i++) {
		time2[i] = kernel[i];
	}
	time2[0] = impulse;
	fftw_free(kernel);
	fftw_complex *freq2 = fftw_malloc(sizeof(fftw_complex) * ((dft_size/2) + 1));
	fftw_plan pf2 = fftw_plan_dft_r2c_1d(dft_size, time2, freq2, FFTW_ESTIMATE);
	fftw_execute(pf2);

	/** multiply first DFT and second DFT **/
	fftw_complex *freq3 = fftw_malloc(sizeof(fftw_complex) * ((dft_size/2) + 1));
	for (int i = 0; i < ((dft_size/2) + 1); i++) {
		/** ReX[f]*ReH[f] - ImX[f]*ImH[f]
		 *  ImX[f]*ReH[f] + ReX[f]*ImH[f] **/
		freq3[i][0] = (freq1[i][0] * freq2[i][0]) - (freq1[i][1] * freq2[i][1]);
		freq3[i][1] = (freq1[i][1] * freq2[i][0]) + (freq1[i][0] * freq2[i][1]);
	}
	fftw_destroy_plan(pf1);
	fftw_destroy_plan(pf2);
	fftw_free(time1); fftw_free(freq1);
	fftw_free(time2); fftw_free(freq2);

	/** inverse DFT on product **/
	double *time3 = fftw_malloc(sizeof(double) * dft_size);
	fftw_plan pb = fftw_plan_dft_c2r_1d(dft_size, freq3, time3, FFTW_ESTIMATE);
	fftw_execute(pb);

	/** gather just the real part of the time3 output **/
	int64_t *time4 = malloc(sizeof(int64_t) * dft_size);
	for (int i = 0; i < dft_size; i++) {
		time4[i] = (int64_t) time3[i];
	}

	fftw_free(time3); fftw_free(freq3);
	fftw_destroy_plan(pb);

	return time4;
}


