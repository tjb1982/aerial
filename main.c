#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdint.h>
#include <fftw3.h>
#include "tools.h"

void
test(struct wav *signal1, struct wav *signal2) {
	int i = 0, dft_size = 1000;
	fftw_complex *in = fftw_malloc(sizeof(fftw_complex) * dft_size);
	fftw_complex *out = fftw_malloc(sizeof(fftw_complex) * dft_size);
	fftw_plan p = fftw_plan_dft_1d(dft_size, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

	for (; i < dft_size; i++) {
		in[i][0] = signal1->data[10000 + i];
		in[i][1] = 0;
	}

	fftw_execute(p);

	fftw_destroy_plan(p);
	fftw_free(in); fftw_free(out);
}

void
usage() {
	printf("usage: alysha signal1.wav offset(sec) length(sec) signal2.wav offset(sec) length(sec) srate impulse outfile.wav\n");
	exit(0);
}

int
main(int argc, const char *argv[]) {

	if (argc != 10) usage();

	FILE *file1 = fopen(argv[1], "r");
	FILE *file2 = fopen(argv[4], "r");
	struct wav *signal1 = new_wav_from_file(file1);
	struct wav *signal2 = new_wav_from_file(file2);

	fclose(file1); fclose(file2);

	/** 
	* Convert signal2 and signal1 data into int64_t so that it can be manipulated
	* without clipping
	**/
	int64_t *signal1_data = calloc(signal1->num_frames, sizeof(int64_t));
	for (int i = 0; i < signal1->num_frames; i++)
		signal1_data[i] = signal1->data[i];

	int64_t *signal2_data = calloc(signal2->num_frames, sizeof(int64_t));
	for (int i = 0; i < signal2->num_frames; i++)
		signal2_data[i] = signal2->data[i];

//	normalize(signal1_data, signal1->num_frames, (1<<15) -1);

    fprintf(stderr, "%s::%f::%i\n", argv[6], atof(argv[6]), (int)(signal1->header->srate * atof(argv[6])));
	int64_t *filtered_data = convolve(
		signal1_data,
		signal1->header->srate*atof(argv[2]),
		signal1->header->srate*atof(argv[3]),
		signal2_data,
		signal2->header->srate*atof(argv[5]),
		signal1->header->srate*atof(argv[6]),
		atoi(argv[7]), // srate
		(1<<atoi(argv[8])) // impulse
	);

	struct wav *outwav = new_wav(new_wav_header(44100, 1, 16), atof(argv[3]));

//	window64(filtered_data, outwav->num_frames, WELCH);

	normalize(filtered_data, outwav->num_frames, (1<<15) - 1);
	for (int i = 0; i < outwav->num_frames; i++)
		outwav->data[i] = (int16_t) filtered_data[i];

	free(filtered_data);

	write_wav(outwav, argv[9]);

	free(signal1_data);
	free(signal2_data);
	free_wav(signal2);
	free_wav(signal1);
	return EXIT_SUCCESS;
}

