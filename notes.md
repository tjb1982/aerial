I think if we want to be able to use this as a way to apply IR reverb to a recording, we have to plan for there to be any number of signals that represent a variety of spatial locations, etc. Maybe the best way to do that is to just only accept two mono signals, and then leave it up to the user to put them together in audacity or something else like that. For instance, the IR I downloaded has four channels which appear to be front-right, front-left, back-right, back-left, or something akin to that. But I probably only care about making a stereo signal from that, so if I wanted to get that done, everything would depend on what the original signal looked like. It coud be stereo, it could be mono. If stereo, then obviously I would want to convolve one side at a time, since the IR is a list of mono signals. So we would need to be able to have a CLI argument that allows us to select the channel number from the input wav file, which wouldn't be a problem, I don't think. So I think what we want to do is create program that takes a wav file of any number of channels, etc., and another wav file of any number of channels, and output a single mono wav file. We could also just output a raw file without any header at all, I think, as long as we knew what the encoding was (Audacity could import it just fine). But since we already have the Wav capability, it would probably be more useful to output that format.

But assuming we always output a mono wav file, then it would be possible to write a shell script to process more than one channel. So for instance in the case where we have a mono source and four IRs like we have with this iPhone recording from the latest rehearsal, then I think we could write a script that looks like this:

```bash
outfile="test"

for ir in ir1.wav ir2.wav ir3.wav ir4.wav; do
    alysha infile.wav --channel=0 $ir --channel 0 $outfile-$ir
done

```

We can probably assume that the sample-rate and bits should be taken from the infile, but maybe these can be additional parameters for the outfile. The question becomes then how do you reconcile them when they are different. That adds some complexity I wish I didn't have to deal with right away.

The other thing is that in this case we have IRs that are just raw data of a certain format, so either we have to convert them in Audacity or maybe we need another utility that can convert them the same way Audacity does except with a CLI, e.g.:

```bash
for ir in ir1 ir2 ir3 ir4; do
    raw2wav $ir --sr=44100 --byte-order=big --encoding=int24
done
```

It seems like something like that could already be available, honestly. It seems crazy that it wouldn't be.

hiatus. Turns out `sox` can do this, e.g.:

```bash
for ir in ir1 ir2 ir3 ir4; do
    sox -t raw -r 44100 -b 24 -e signed-integer -B -c 1 $ir ./${ir}.wav
done
```

And then once you get a bunch of mono wav files, you can load them into Audacity and deal with the rest of the editing there. 
